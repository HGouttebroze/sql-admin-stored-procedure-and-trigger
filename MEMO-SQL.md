# SQL - Bases de données relationnelles

+ Conception, Administration, Optimisation

## Procedures stockées

+ Les procédures stockées sont des fonctions « coté serveur de base de données » : 
    + Elles sont développées « sur mesure » pour accomplir un traitement
    + Elles font pleinement partie de la structure de la base de données
    + Elles peuvent être créées, modifiées et supprimées à l'aide de `CREATE`, `ALTER` et `DROP`
    Elles peuvent être exécutées :
        → Exemple d’appel : `EXECUTE NomProcedure;`

## But 

+ Le but d’une procédure stockée est d’exécuter un traitement personnalisé :

+ Il peut s’agir d’un traitement :
    + de lecture (exemple : export statistiques complexe)
    + d’insertion (exemple : facturation de fin de mois)
    + de mise à jour (exemple : augmentation tarifaire sélective)
    + de toute autre nature (exemple : envois de mail, sauvegardes, purges, etc.)

+ Les procédures stockées permettent aussi de garder en mémoire des requêtes « support » utilisés a des fins de monitoring

```sql
DELIMITER |
CREATE PROCEDURE NomProcedure()
BEGIN
--(traitement);
END|

DELIMITER |
CREATE PROCEDURE NomProcedure(INT parametre)
BEGIN
--(traitement utilisant le paramètre);
END|
``` 

+ ligne 30 : Puisque la partie traitement de la procédure utilise le point-virgule, il est nécessaire de changer le délimiteur pour marquer la fin de la procédure.

## Exemple pas à pas d'une insertion de 40 à 60 données aléatoires dans une table « Entreprise »

1. Part 1 : 
```sql
BEGIN


END
```
2. Part 2 :
```sql
BEGIN

    INSERT INTO Entreprise(nom)
    VALUES ('Un nom...');

END
```

+ Part 3 : 
```sql
BEGIN

    WHILE         DO -- On boucle


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

    END WHILE;

END
```

+ Part 4 : 
```sql
BEGIN
    SET @i = 0;

    WHILE @i < 40   DO -- On boucle 40 fois


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

        SET @i = @i + 1;

    END WHILE;

END
```

+ Part 5 : 
```sql
BEGIN

    SET @maxi = 40;
    SET @i = 0;

    WHILE @i < 40   DO -- On boucle 40 fois


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

        SET @i = @i + 1;

    END WHILE;

END
```


+ Part 6 : 
```sql
BEGIN
   
    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
 


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

        SET @i = @i + 1;

    END WHILE;

END
```

+ Part 7 : 
```sql
BEGIN
   
    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = -- Comment faire une chaine aléatoire ?


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

        SET @i = @i + 1;

    END WHILE;

END
```
+ Part 8 : 
```sql
BEGIN
   
    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = CONV(FLOOR(RAND() * 99999999999999), 20, 36); -- Chaine aléatoire


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

        SET @i = @i + 1;

    END WHILE;

END
```

+ Part 9 : 
```sql
BEGIN
   
    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = CONV(FLOOR(RAND() * 99999999999999), 20, 36); -- Chaine aléatoire


        INSERT INTO Entreprise(nom)
        VALUES ('Un nom...');

        SET @i = @i + 1;

    END WHILE;

    SELECT CONCAT(@i, ' entreprises ajoutées') AS Resultat;

END
```
+ Part 10 : 

```sql
BEGIN

    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = CONV(FLOOR(RAND() * 99999999999999), 20, 36); -- Chaine aléatoire
        SET @nbSalariesTemp = 1 + RAND() * 249; -- Nombre aléatoire entre 1 et 250

        INSERT INTO Entreprise(nom, nbSalaries)
        VALUES (@nomTemp, @nbSalariesTemp);

        SET @i = @i + 1;
    END WHILE;

    SELECT CONCAT(@i, ' entreprises ajoutées') AS Resultat;

END
```
+ Part 11 : 

```sql
BEGIN

    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = CONV(FLOOR(RAND() * 99999999999999), 20, 36); -- Chaine aléatoire
        SET @nbSalariesTemp = 1 + RAND() * 249; -- Nombre aléatoire entre 1 et 250
        SET @estPubliqueTemp = IF(RAND() < 0.10, 1, 0); -- Est publique dans 10 % des cas

        INSERT INTO Entreprise(nom, nbSalaries, estPublique)
        VALUES (@nomTemp, @nbSalariesTemp, @estPubliqueTemp);

        SET @i = @i + 1;
    END WHILE;

    SELECT CONCAT(@i, ' entreprises ajoutées') AS Resultat;

END
```
+ Part 12 : 

```sql
BEGIN

    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = CONV(FLOOR(RAND() * 99999999999999), 20, 36); -- Chaine aléatoire
        SET @nbSalariesTemp = 1 + RAND() * 249; -- Nombre aléatoire entre 1 et 250
        SET @estPubliqueTemp = IF(RAND() < 0.10, 1, 0); -- Est publique dans 10 % des cas
        SET @dateFondaTemp = CURDATE() - INTERVAL (RAND() * 300) DAY; -- Date aléatoire
        
        INSERT INTO Entreprise(nom, nbSalaries, estPublique, dateFondation)
        VALUES (@nomTemp, @nbSalariesTemp, @estPubliqueTemp, @dateFondaTemp);

        SET @i = @i + 1;
    END WHILE;

    SELECT CONCAT(@i, ' entreprises ajoutées') AS Resultat;

END
```
+ Part 13 : 

```sql
BEGIN

    SET @maxi = 40 + RAND() * 20; -- Nombre aléatoire entre 40 et 60 (60 - 40 = 20)
    SET @i = 0;

    WHILE @i < @maxi DO -- On boucle autant de fois que demandé
        SET @nomTemp = CONV(FLOOR(RAND() * 99999999999999), 20, 36); -- Chaine aléatoire
        SET @nbSalariesTemp = 1 + RAND() * 249; -- Nombre aléatoire entre 1 et 250
        SET @estPubliqueTemp = IF(RAND() < 0.10, 1, 0); -- Est publique dans 10 % des cas
        SET @dateFondaTemp = CURDATE() - INTERVAL (RAND() * 300) DAY; -- Date aléatoire
        SET @idDptTemp = (SELECT idDepartement FROM Departement
        ORDER BY RAND() LIMIT 1); -- ID de département aléatoire

        INSERT INTO Entreprise(nom, nbSalaries, estPublique, dateFondation, idDepartement)
        VALUES (@nomTemp, @nbSalariesTemp, @estPubliqueTemp, @dateFondaTemp, @idDptTemp);

        SET @i = @i + 1;
    END WHILE;

    SELECT CONCAT(@i, ' entreprises ajoutées') AS Resultat;

END
```
## 